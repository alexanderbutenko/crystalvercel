import { Container } from 'inversify';
import { FanscoreAuthProvider } from 'fanx-ui-framework/general/services/js/authentication/auth-providers/fanscore';
import { IAuthProvider } from 'fanx-ui-framework/general/services/js/authentication/auth-providers';
import TYPES from './types';

const inversifyContainer = new Container();
inversifyContainer
    .bind<IAuthProvider>(TYPES.IAuthProvider)
    .to(FanscoreAuthProvider)
    .inSingletonScope();

export default inversifyContainer;
