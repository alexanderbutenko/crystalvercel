module.exports = {
    sassOptions: {
        prependData: '@import \'fanx-ui-framework/general/scss/settings/index.scss\';',
    },
    target: 'serverless',
};
