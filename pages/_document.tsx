import React from 'react';
import Document, { Html, Head, Main, NextScript } from 'next/document';

export default class _Document extends Document {
    render(): JSX.Element {
        return (
            <Html className="html">
                <Head />
                <body className="body">
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}
