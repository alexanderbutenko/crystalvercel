import React from 'react';
import Head from 'next/head';
import Error from 'next/error';
import { useRouter } from 'next/router';
import { GetStaticProps, GetStaticPaths } from 'next';
import { AxiosResponse, AxiosError } from 'axios';
import Custom404 from 'pages/404';
import { Article } from 'fanx-ui-framework/components/article';
import { Endpoints } from 'fanx-ui-framework/general/app/endpoints';
import { axiosNode } from 'fanx-ui-framework/general/services';
import {
    IArticle,
    IArticlePage,
    IArticlesResponse,
    IArticlesQsParams,
    IArticleResponse,
} from 'fanx-ui-framework/general/interfaces/articleIntrfaces';

interface IGetStaticProps {
    props: {
        article: IArticlePage | null;
        statusCode: boolean | number;
    };

    revalidate?: number;
}

const ArticlePage = ({ article, statusCode }: IArticlePage): JSX.Element => {
    const router = useRouter();

    if (router.isFallback) {
        return <div>Loading...</div>;
    }

    if (statusCode) {
        return <Error statusCode={statusCode} />;
    }

    if (!article) {
        return (
            <>
                <Head>
                    <meta name="robots" content="noindex" />
                </Head>
                <Custom404 />
            </>
        );
    }

    return (
        <>
            <Head>
                <title>{`Next.js - ${article.heroMedia.title}`}</title>
            </Head>
            <section>
                <Article article={article} />
            </section>
        </>
    );
};

const allArticlesParams: IArticlesQsParams = {
    page: 0,
    size: 50, // todo: 50000
    sort: '-publishDate',
    categorySlug: 'news',
    categoryId: '94f1c1eb-9434-47c3-99a7-4dd521020013',
    clientId: 'PRO14',
};

const getAllArticles = async (): Promise<IArticle[]> => {
    const response: AxiosResponse<IArticlesResponse> = await axiosNode.get(Endpoints.Articles, {
        params: allArticlesParams,
    });
    const { data } = response.data;
    const articles = data.articles;

    return articles.reduce((result: IArticle[], article: IArticle): IArticle[] => {
        if (article.displayCategory) {
            result.push(article);
        }

        return result;
    }, []);
};

export const getStaticPaths: GetStaticPaths = async () => {
    const allArticles = await getAllArticles();

    return {
        paths: allArticles.map(({ displayCategory, slug }: IArticle) => {
            return {
                params: {
                    category: displayCategory?.text,
                    slug,
                },
            };
        }),

        fallback: true,
    };
};

export const getStaticProps: GetStaticProps = async ({ params }): Promise<IGetStaticProps> => {
    let article: IArticle | null = null;
    let statusCode: boolean | number = false;

    await axiosNode

        .get(`${Endpoints.ArticleBySlug}/${params?.slug}`, {
            params: {
                clientId: 'PRO14',
            },
        })
        .then(({ data }: AxiosResponse<IArticleResponse>): void => {
            article = data.data.article;
        })
        .catch(({ response }: AxiosError): void => {
            if (response) {
                statusCode = response.status;
            }
        });

    return {
        props: {
            article,
            statusCode,
        },

        revalidate: 60,
    };
};

export default ArticlePage;
